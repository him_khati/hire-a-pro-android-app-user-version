package hireapro.himanshu.hireapro;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import hireapro.himanshu.hireapro.gps.GPSTracker;

public class SplashActivity extends AppCompatActivity {
    public static SharedPreferences sp;
    public static GPSTracker gpsTracker;
    int PERMISSION_REQUEST_CODE = 101;
    int SPLASH_TIME_OUT = 2200;
    String requiredPermissionsList[] = {Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gpsTracker = new GPSTracker(SplashActivity.this);
        setContentView(R.layout.splashscreen);

        ProgressBar pb = new ProgressBar(this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkRuntimePermissions())
                    startLoginActivity();
                else
                    requestRuntimePermissions();
            }
        }, SPLASH_TIME_OUT);


    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    public boolean checkRuntimePermissions() {

        boolean result = false;
        for (String permission : requiredPermissionsList) {

            if (ContextCompat.checkSelfPermission(SplashActivity.this, permission) == PackageManager.PERMISSION_GRANTED)
                result = true;
            else
                result = false;
        }
        return result;

    }

    private void requestRuntimePermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(requiredPermissionsList, PERMISSION_REQUEST_CODE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean res = false;
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int g : grantResults) {
                if (g == PackageManager.PERMISSION_GRANTED)
                    res = true;
                else
                    res = false;
            }
            if (res)
                startLoginActivity();
            else
                requestRuntimePermissions();
        }
    }


}
