package hireapro.himanshu.hireapro.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hireapro.himanshu.hireapro.R;
import hireapro.himanshu.hireapro.UploadImageActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {
View rootView;
    ImageView userImage;
    TextView reviewsTextView,listAsProfessionalTextView,updateAddressTextView,changePasswordTextView,logoutTextView,setupProfileTextView;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_settings, container, false);
        initializeComponents();
        return rootView;
    }

    private void initializeComponents() {
        userImage =(ImageView) rootView.findViewById(R.id.setting_fragment_user_image);
        reviewsTextView =(TextView) rootView.findViewById(R.id.setting_fragment_review);
        listAsProfessionalTextView =(TextView) rootView.findViewById(R.id.setting_fragment_list_youself);
        updateAddressTextView =(TextView) rootView.findViewById(R.id.setting_fragment_add_address);
        changePasswordTextView =(TextView) rootView.findViewById(R.id.setting_fragment_change_password);
        logoutTextView =(TextView) rootView.findViewById(R.id.setting_fragment_logout);
        setupProfileTextView =(TextView) rootView.findViewById(R.id.setting_fragment_setup_profile);

        userImage.setOnClickListener(this);
        reviewsTextView.setOnClickListener(this);
        listAsProfessionalTextView.setOnClickListener(this);
        updateAddressTextView.setOnClickListener(this);
        changePasswordTextView.setOnClickListener(this);
        logoutTextView.setOnClickListener(this);
        setupProfileTextView.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting_fragment_user_image:
                startActivity(new Intent(getActivity(), UploadImageActivity.class));
                break;
            case R.id.setting_fragment_review:

                break;
            case R.id.setting_fragment_list_youself:

                break;
            case R.id.setting_fragment_add_address:

                break;
            case R.id.setting_fragment_change_password:

                break;
            case R.id.setting_fragment_logout:
                break;
            case R.id.setting_fragment_setup_profile:

                break;
        }

    }
}
