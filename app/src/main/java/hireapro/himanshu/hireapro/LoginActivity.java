package hireapro.himanshu.hireapro;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import hireapro.himanshu.hireapro.dataclass.User;
import hireapro.himanshu.hireapro.sharedpref.Sharedpref;
import hireapro.himanshu.hireapro.web.AsyncRequest;
import hireapro.himanshu.hireapro.web.Url;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {

    private String url;


    private User user;
    //views
    private EditText emailPhoneEditText, passwordEditText;
    private Button loginButton;
    private SwitchCompat remembermeSwitch;
    private TextView forgotPasswordTextView, createAccountTextView;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;

    private HashMap<String, String> params = new HashMap<String, String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Sharedpref(this);
        if(Sharedpref.isUserLoggedIn())
        {
            Sharedpref.loadUserDataFromSharedPref();
            Intent i = new Intent(LoginActivity.this, HomeScreenActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
        setContentView(R.layout.activity_login);
        intializeComponents();
    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch (id) {
            case R.id.login_button:

                if (validateEmail() && validatePassword()) {
                    getUserData();
                    prepareUrl();
                    setParameters();
                    new AsyncRequest(LoginActivity.this, url, AsyncRequest.GET, params).execute();
                }
                break;
            case R.id.create_account_textview:

                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
                break;
            case R.id.forgot_password_textview:
                // your code for button2 here
                break;


        }
    }

    private void setParameters() {
        params.put("phone_no", user.getPhoneNumber() + "");
        params.put("password", user.getPassword());

    }

    private boolean checkRememberMe() {
        if (remembermeSwitch.isChecked())
            return true;
        else
            return false;
    }


    private void prepareUrl() {

        url = Url.SERVER_URL;
        url = url + "/user/loginuser_with_phoneno.php";

    }


    private void getUserData() {
        user.setUserType(User.NORMAL_USER);
        user.setPhoneNumber(Long.valueOf(emailPhoneEditText.getText().toString()));
        user.setPassword(passwordEditText.getText().toString());
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateEmail() {
        if (emailPhoneEditText.getText().toString().trim().isEmpty()) {
            inputLayoutEmail.setError("Please Enter Email or Phone no");
            requestFocus(emailPhoneEditText);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        if (passwordEditText.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError("Please enter password");
            requestFocus(passwordEditText);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }


    private void intializeComponents() {
        emailPhoneEditText = (EditText) findViewById(R.id.phone_email_edittext);
        passwordEditText = (EditText) findViewById(R.id.password_edittext);
        loginButton = (Button) findViewById(R.id.login_button);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_emailphone);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);

        remembermeSwitch = (SwitchCompat) findViewById(R.id.remember_me_toggle);
        forgotPasswordTextView = (TextView) findViewById(R.id.forgot_password_textview);
        createAccountTextView = (TextView) findViewById(R.id.create_account_textview);
        user = new User();
        loginButton.setOnClickListener(this);
        createAccountTextView.setOnClickListener(this);

    }

    @Override
    public void processResponse(JSONObject response) {
        if (response != null) {
            try {
                int stat = response.getInt("status");
                if (stat == 200) {
                    User.setID(response.getString("user_id"));
                    User.setName(response.getString("user_name"));
                    if (remembermeSwitch.isChecked())
                        Sharedpref.setRememberMe(true);
                    else
                        Sharedpref.setRememberMe(false);
                    Intent i = new Intent(LoginActivity.this, HomeScreenActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(LoginActivity.this, "Login Error", Toast.LENGTH_SHORT).show();

                }

            } catch (JSONException e) {
                Log.e("LoginActivity", "processResponse :JSONException, " + e.toString());
            }
        }
    }


}
