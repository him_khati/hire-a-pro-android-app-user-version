package hireapro.himanshu.hireapro.dataclass;

import android.graphics.Bitmap;

import java.io.Serializable;

public class User implements Serializable {

    //User Type contants
    public static final byte NORMAL_USER = 0;
    public static final byte PROFESSIONAL = 1;
    public static final byte ADMIN = 2;
    private static String id, name, emailID, longAddress, profilePictureURL, shortAddress;
    private static long phoneNumber;
    private static String userType;
    private static float locationLatitude, locationLongitude;
    private static transient Bitmap userImage, userProfile_mini;
    private static boolean favorite;
    private static String password;

    public static String getUserType() {

        return userType;

    }

    public static void setUserType(byte userCode) {
        String type = null;
        switch (userCode) {
            case 0:
                userType = "NORMAL_USER";
                break;
            case 1:
                userType = "PROFESSIONAL";
                break;
            case 2:
                userType = "ADMIN";
                break;
        }
    }

    public static String getShortAddress() {
        return shortAddress;
    }

    public static void setShortAddress(String shortAddress) {
        User.shortAddress = shortAddress;
    }

    public static String getID() {
        return id;
    }

    public static void setID(String id) {
        User.id = id;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        User.name = name;
    }

    public static String getEmailID() {
        return emailID;
    }

    public static void setEmailID(String emailID) {
        User.emailID = emailID;
    }

    public static String getLongAddress() {
        return longAddress;
    }

    public static void setLongAddress(String longAddress) {
        User.longAddress = longAddress;
    }

    public static String getProfilePictureURL() {
        return profilePictureURL;
    }

    public static void setProfilePictureURL(String profilePictureURL) {
        User.profilePictureURL = profilePictureURL;
    }

    public static long getPhoneNumber() {
        return phoneNumber;
    }

    public static void setPhoneNumber(long phoneNumber) {
        User.phoneNumber = phoneNumber;
    }

    public static float getLocationLatitude() {
        return locationLatitude;
    }

    public static void setLocationLatitude(float locationLatitude) {
        User.locationLatitude = locationLatitude;
    }

    public static float getLocationLongitude() {
        return locationLongitude;
    }

    public static void setLocationLongitude(float locationLongitude) {
        User.locationLongitude = locationLongitude;
    }

    public static Bitmap getUserImage() {
        return userImage;
    }

    public static void setUserImage(Bitmap userImage) {
        User.userImage = userImage;
    }

    public static boolean isFavorite() {
        return favorite;
    }

    public static void setFavorite(boolean favorite) {
        User.favorite = favorite;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        User.password = password;
    }
}
