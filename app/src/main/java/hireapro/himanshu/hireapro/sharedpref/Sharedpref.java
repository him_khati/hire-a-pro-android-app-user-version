package hireapro.himanshu.hireapro.sharedpref;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import hireapro.himanshu.hireapro.LoginActivity;
import hireapro.himanshu.hireapro.SplashActivity;
import hireapro.himanshu.hireapro.dataclass.User;

/**
 * Created by for u on 6/22/2017.
 */

public class Sharedpref {
    //Key name to be used when storing data usign SHaredPreference
    public static final String IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN";
    public static final String ID = "ID";
    public static final String USER_TYPE = "USER_TYPE";
    public static final String NAME = "NAME";
    public static final String ADDRESS_LONG = "ADDRESS_LONG";
    public static final String ADDRESS_SHORT = "ADDRESS_SHORT";
    public static final String USER_LATITUDE = "USER_LATITUDE";
    public static final String USER_LONGITUDE = "USER_LONGITUDE";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String PREF_NAME = "HIRE_PREF";


    Context mContext;//Name of the user
    public static boolean isUserLoggedIn = false;        //true - remember login ,false - do not remember login

    final int PRIVATE_MODE = 0;// sahred pref mode
    public static SharedPreferences.Editor editor;
    static SharedPreferences sharedpref;//Shared pref editor



    public Sharedpref(Context mContext) {
        this.mContext = mContext;
        sharedpref =    mContext.getApplicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedpref.edit();
    }

    //Check if user opted for remember login during last login
    public static boolean isUserLoggedIn() {
        isUserLoggedIn = sharedpref.getBoolean(IS_USER_LOGGED_IN, false);
        return isUserLoggedIn;
    }


    public static void loadUserDataFromSharedPref()
    {
    User.setName(sharedpref.getString(NAME,""));
        User.setID(sharedpref.getString(ID,""));
        User.setUserType(User.NORMAL_USER);
        User.setPhoneNumber(sharedpref.getLong(PHONE_NUMBER,0));
    }


    //Setting the remember me data in shredpref
    public static void setRememberMe(boolean option) {

        if (option) {
            editor.putBoolean(IS_USER_LOGGED_IN, option);
            editor.putString(ID, User.getID());
            editor.putString(USER_TYPE, User.getUserType());
            editor.putString(NAME, User.getName());
            editor.putLong(PHONE_NUMBER,User.getPhoneNumber());
            editor.commit();
        } else {
            editor.clear();
            editor.commit();
        }
        Log.d("SetRemmberMecalled", "id : " + User.getID());
        Log.d("Remember Login", sharedpref.getBoolean(IS_USER_LOGGED_IN, false) + "");
    }

    public static void persistLocation(float latitude, float lon) {
        editor.putFloat(USER_LATITUDE,User.getLocationLatitude() );
        editor.putFloat(USER_LONGITUDE,User.getLocationLongitude());
        editor.commit();
    }

    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(mContext, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        mContext.startActivity(i);
    }

    public static float[] getPersistedLocation() {

        return new float[]{sharedpref.getFloat(USER_LATITUDE, 0.0f), sharedpref.getFloat(USER_LONGITUDE, 0.0f)};
    }


    public static void persistPhoneNumber(long phoneNumber) {
        editor.putLong(PHONE_NUMBER,User.getPhoneNumber());
        editor.commit();
    }

    //retrives persistently stored Phone number
    public static long getPersistedPhoneNumber() {
        return sharedpref.getLong(PHONE_NUMBER, 0);
    }

    //Stores locality name persistently
    public static void persistAddressShort() {
        editor.putString(ADDRESS_SHORT,User.getShortAddress());
        editor.commit();
    }

    //retrives persistently stored locality name
    public static String getPersistedAddressShort() {
        return sharedpref.getString(ADDRESS_SHORT, "");
    }

    //Stores full address persistently
    public static void persistLongAddress() {
        editor.putString(ADDRESS_LONG, User.getLongAddress());
        editor.commit();
    }

    //retrives persistently stored address name
    public static String getPersistedLongAddress() {
        return sharedpref.getString(ADDRESS_LONG, "");
    }



}
