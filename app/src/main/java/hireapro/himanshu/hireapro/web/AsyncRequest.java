package hireapro.himanshu.hireapro.web;

/**
 * Created by for u on 6/19/2017.
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

public class AsyncRequest extends AsyncTask<Void, Integer, JSONObject> {

    OnAsyncRequestComplete callerActivity;
    Context context;
    public static final byte GET = 0;
    public static final byte POST = 1;
    private HashMap<String, String> parameters = null;
    ProgressDialog pDialog = null;
    String connectionMethod;
    final int READ_TIME_OUT = 15000;
    final int CONNECTION_TIME_OUT = 15000;

    StringBuilder url;
    HttpURLConnection httpURLConnection = null;
    URL urlObject;
    InputStream inputStream;
    BufferedReader bufferedReader;
    StringBuffer stringBuffer;

    //Interface To be implemented
    public interface OnAsyncRequestComplete {
        public void processResponse(JSONObject response);
    }

    // Three Constructors
    public AsyncRequest(Activity callerActivity,String url,Byte method, HashMap<String, String> parameters) {
        this.callerActivity = (OnAsyncRequestComplete) callerActivity;
        this.context = callerActivity;
        this.url = new StringBuilder(url);
        this.connectionMethod = getConnectionMethod(method);
        this.parameters = parameters;
        Log.d("Param","recieved");
    }

/*
    public AsyncRequest(Activity callerActivity, String connectionMethod) {
        this.callerActivity = (OnAsyncRequestComplete) callerActivity;
        this.context = callerActivity;
        this.connectionMethod = connectionMethod;
    }

    public AsyncRequest(Activity callerActivity) {
        this.callerActivity = (OnAsyncRequestComplete) callerActivity;
        this.context = callerActivity;
    }
*/
    private String getConnectionMethod(byte method) {
        String type = null;
        switch (method) {
            case 0:
                type = "GET";
                break;
            case 1:
                type = "POST";
                break;
        }
        return type;
    }

    public void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        pDialog.setCancelable(false);
        initiateConnection();
    }

    private void initiateConnection() {
        try {
            if(connectionMethod.equals("POST"))
            {
                urlObject = new URL(new String(url));
                httpURLConnection = (HttpURLConnection) urlObject.openConnection();
                httpURLConnection.setRequestMethod("POST");
            }
            else
                if(connectionMethod.equals("GET"))
                {
                    setUpGETparameters();
                    urlObject = new URL(new String(url));
                    httpURLConnection = (HttpURLConnection) urlObject.openConnection();
                  //  httpURLConnection.setRequestMethod("GET");

                }
          //  httpURLConnection.setDoInput(true);
           // httpURLConnection.setDoOutput(true);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setConnectTimeout(15000);

            execute();
        }
        catch (Exception e)
        {
            System.err.print(e.getMessage());
        }
    }

    private void setUpGETparameters() {
        url = url.append("?");
        for (String name: parameters.keySet()){

            String key =name.toString();
            String value = parameters.get(name).toString();
            url.append(key);
            url.append("=");
            url.append(value);
            url.append("&");
            Log.d(key,value);
        }
        url.deleteCharAt((url.length()-1));
        Log.d("url",new String(url));
    }
    private void setUpPOSTParameters() throws IOException {
        Uri.Builder builder = new Uri.Builder();

        for (String name: parameters.keySet()){

            String key =name.toString();
            String value = parameters.get(name).toString();
            builder.appendQueryParameter(key,value);
        }

        String query = builder.build().getEncodedQuery();
        OutputStream os = httpURLConnection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(query);
        writer.flush();
        writer.close();
        os.close();
    }

    public JSONObject doInBackground(Void... v) {
        Log.d("Param","Start");
        try {

            httpURLConnection.connect();

            inputStream = httpURLConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            stringBuffer = new StringBuffer();

            String line = "";

            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
            String response = stringBuffer.toString();
            if (response!=null)
            Log.d("AsyncRequest, response", response);
            return (new JSONObject(response));

        } catch (Exception e) {
            Log.e("AsyncRequest, Con Err:", e.toString());
        }

        return null;

    }

    public void onProgressUpdate(Integer... progress) {
        // you can implement some progressBar and update it in this record
        // setProgressPercent(progress[0]);
    }

    public void onPostExecute(JSONObject response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        callerActivity.processResponse(response);
    }

    protected void onCancelled(JSONObject response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        callerActivity.processResponse(response);
    }


}

