package hireapro.himanshu.hireapro.gps;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class Geocode {

    Context mContext;
    Geocoder geocoder;
    List<Address> addressList;

    public Geocode(Context mContext, double latitude, double longitude) throws IOException {
        this.mContext = mContext;
        geocoder = new Geocoder(mContext);

        addressList = geocoder.getFromLocation(latitude, longitude, 1);
    }


    public String getStateName() throws NullPointerException {
        return addressList.get(0).getAdminArea();
    }

    public String getCountryCode() throws NullPointerException {
        return addressList.get(0).getCountryCode();
    }

    public String getDistrictName() throws NullPointerException {
        return addressList.get(0).getSubAdminArea();
    }

    public String getCountryName() throws NullPointerException {
        return addressList.get(0).getCountryName();
    }

    public String getPostalCode() throws NullPointerException {
        return addressList.get(0).getPostalCode();
    }

    public String getLocality() throws NullPointerException {
        return addressList.get(0).getLocality();
    }

    public Locale getLocale() throws NullPointerException {
        return addressList.get(0).getLocale();
    }

    public String getSubLocality() throws NullPointerException {
        return addressList.get(0).getSubLocality();
    }

    public String getFullAddress() throws NullPointerException {
        return addressList.get(0).getSubLocality();
    }

    public String getShortAddress() throws NullPointerException {
        return addressList.get(0).getAddressLine(0);
    }
}
